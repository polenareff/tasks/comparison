from http.server import BaseHTTPRequestHandler, HTTPServer
import cgi, cv2, json, numpy as np

from .algo import compare

class Server(BaseHTTPRequestHandler):

    @classmethod
    def raw2img(cls, raw):
        arr = np.frombuffer(raw, dtype=np.uint8)
        img = cv2.imdecode(arr, cv2.IMREAD_COLOR)
        if img is None:
            raise Exception('Invalid image data provided!')
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA)
        return img

    def do_POST(self):
        content_length = int(self.headers.get('Content-Length'))
        try:
            content_type, pdict = \
                    cgi.parse_header(self.headers.get('Content-Type'))
            pdict['boundary'] = pdict['boundary'].encode()
            pdict['CONTENT-LENGTH'] = content_length
            if not content_type == 'multipart/form-data':
                raise Exception('Only multipart form post allowed!')
            data = cgi.parse_multipart(self.rfile, pdict)

            img_a = Server.raw2img(data['img_a'][0])
            img_1 = Server.raw2img(data['img_1'][0])

            response = compare(img_a, img_1)
            response_raw = json.dumps(response).encode()
        except:
            self.send_response(400)
            self.end_headers()
        else:
            self.send_response(200)
            self.end_headers()
            self.wfile.write(response_raw)

httpd = HTTPServer(('0.0.0.0', 6666), Server)
httpd.serve_forever()

